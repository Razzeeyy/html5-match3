module.exports = {
  "extends": [
    "standard",
    "eslint:recommended"
  ],

  "rules": {
    "strict": ["error", "global"]
  },

  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "script"
  },

  "env": {
    "commonjs": true,
    "browser": true,
    "es6": true
  }
};
