"use strict";

const CANVAS_ID = "game-canvas";
const LOADER_BASE_URL = "assets/";

const PIXI = require("pixi.js");
const assets = require("./app/defs/assets");
const StateManager = require("./app/core/StateManager");
const GameState = require("./app/states/GameState");
const events = require("./app/defs/events");
const world = require("./app/defs/world");
const Random = require("./app/core/Random");

let app = new PIXI.Application({ view: document.getElementById(CANVAS_ID) });

let context = {};
context.pixi = PIXI;
context.app = app;
context.loader = app.loader;
context.renderer = app.renderer;
context.screen = app.screen;
context.stage = app.stage;
context.ticker = app.ticker;
context.view = app.view;
context.state = new StateManager(context);
context.resources = null; //will be set later when assets loaded
context.events = events;
context.world = world;
context.random = new Random();

window.addEventListener("resize", () => {
  app.renderer.resize(window.innerWidth, window.innerHeight);
  context.state.invoke("onResize");
});

app.renderer.resize(window.innerWidth, window.innerHeight);
app.start();

app.loader.baseUrl = LOADER_BASE_URL;
Object.entries(assets).forEach(([key, value]) => app.loader.add(key, value)); //queue assets for loading
app.loader.load((loader, resources) => {
  context.resources = resources;
  context.state.setState(new GameState());
  //HACK: because fuck PIXI.Container resize optimizations
  window.setTimeout(() => {
    context.state.invoke("onResize");
  }, 500);
});
