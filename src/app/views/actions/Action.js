"use strict";

module.exports = class Action {
  constructor(boardView) {
    this._boardView = boardView;
  }

  run() {
  }
};
