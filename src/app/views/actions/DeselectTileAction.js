"use strict";

const Action = require("./Action");

module.exports = class DeselectTileAction extends Action {
  constructor(boardView, x, y) {
    super(boardView);
    this.x = x;
    this.y = y;
  }

  run() {
    const tileView = this._boardView._tileViews.get(this.x, this.y);
    tileView.alpha = 1;
  }
}
