"use strict";

const Action = require("./Action");
const TimelineLite = require("gsap").TimelineLite;

module.exports = class SwitchTileAction extends Action {
  constructor(boardView, thisX, thisY, thatX, thatY) {
    super(boardView);
    this.thisX = thisX;
    this.thisY = thisY;
    this.thatX = thatX;
    this.thatY = thatY;
  }

  run() {
    const boardView = this._boardView;
    const tiles = boardView._tileViews;

    const thisTileView = tiles.get(this.thisX, this.thisY);
    const thatTileView = tiles.get(this.thatX, this.thatY);

    tiles.set(this.thatX, this.thatY, thisTileView);
    tiles.set(this.thisX, this.thisY, thatTileView);

    const thisTween = boardView.createTweenForTileView(thisTileView, { tileX: this.thatX, tileY: this.thatY }, 0.15);
    const thatTween = boardView.createTweenForTileView(thatTileView, { tileX: this.thisX, tileY: this.thisY }, 0.15);

    const timeline = new TimelineLite();
    timeline.add(thisTween, 0);
    timeline.add(thatTween, 0);

    return timeline;
  }
}
