"use strict";

const Action = require("./Action");

module.exports = class MoveTileAction extends Action {
  constructor(boardView, fromX, fromY, toX, toY) {
    super(boardView);
    this.fromX = fromX;
    this.fromY = fromY;
    this.toX = toX;
    this.toY = toY;
  }

  run() {
    const boardView = this._boardView;
    const tiles = this._boardView._tileViews;

    const tileView = tiles.get(this.fromX, this.fromY);

    tiles.move(this.fromX, this.fromY, this.toX, this.toY);

    const tween = boardView.createTweenForTileView(tileView, { tileX: this.toX, tileY: this.toY }, 0.3);

    return tween;
  }
}
