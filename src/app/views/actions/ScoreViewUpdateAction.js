"use strict";

const TweenLite = require("gsap").TweenLite;

module.exports = class ScoreViewUpdateAction {
  constructor(scoreView, score) {
    this._scoreView = scoreView;
    this._score = score;
  }

  run() {
    this._scoreView.incrementScore(this._score);

    const scaleJump = 1.5;
    const duration = 0.7;

    this._scoreView.scale.x = this._scoreView.scale.y = scaleJump;
    const tween = TweenLite.to(this._scoreView.scale, duration, {x: 1, y: 1});
    return tween;
  }
};
