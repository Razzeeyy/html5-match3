"use strict";

const Action = require("./Action");

module.exports = class RemoveTileAction extends Action {
  constructor(boardView, x, y) {
    super(boardView);
    this.x = x;
    this.y = y;
  }

  run() {
    const boardView = this._boardView;
    const tileView = this._boardView._tileViews.get(this.x, this.y);

    const tween = boardView.createTweenForTileView(tileView, {
      alpha: 0,
      onComplete: tileView.removeFromBoardView,
      onCompleteScope: tileView
    }, 0.3);

    return tween;
  }
}
