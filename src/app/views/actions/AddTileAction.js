"use strict"

const Action = require("./Action");
const TimelineLite = require("gsap").TimelineLite;

module.exports = class AddTileAction extends Action {
  constructor(boardView, x, y, tileId) {
    super(boardView);
    this.x = x;
    this.y = y;
    this.tileId = tileId;
  }

  run() {
    const boardView = this._boardView;
    const newTileView = boardView.createTileViewForTileTypeId(this.tileId);

    boardView._tileViews.set(this.x, this.y, newTileView);
    boardView.addChild(newTileView);

    const boardWidth = boardView._tileViews.width;
    const boardHeight = boardView._tileViews.height;
    newTileView.setTilePosition(boardWidth/2, this.y-boardHeight*2);

    newTileView.alpha = 0;
    const tween = boardView.createTweenForTileView(newTileView, { tileX: this.x, tileY: this.y, alpha: 1 }, 0.5);

    newTileView.scale.y = 0;
    newTileView.scale.x = 0;

    const scaleTween = boardView.createTweenForTarget(newTileView.scale, {x: 1, y:1}, 0.6);

    const timeline = new TimelineLite();
    timeline.add(tween, 0);
    timeline.add(scaleTween, 0);


    return timeline;
  }
}
