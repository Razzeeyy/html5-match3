"use strict"

const GSAP = require("gsap");
const TweenLite = GSAP.TweenLite;
const PIXI = require("pixi.js");
const TileView = require("./TileView");
const Array2 = require("../core/Array2");
const Action = require("./actions/Action");
const AddTileAction = require("./actions/AddTileAction");
const RemoveTileAction = require("./actions/RemoveTileAction");
const SelectTileAction = require("./actions/SelectTileAction");
const DeselectTileAction = require("./actions/DeselectTileAction");
const SwitchTileAction = require("./actions/SwitchTileAction");
const MoveTileAction = require("./actions/MoveTileAction");

module.exports = class BoardView extends PIXI.Container {
  constructor(context, boardHeight, boardWidth, tileSize) {
    super();

    this._events = context.events;
    this._ticker = context.ticker;

    this.interactive = true;
    this.buttonMode = true;

    this.pointerdown = this.onClicked.bind(this);

    this._resources = context.resources;
    this._tileSize = tileSize;

    this._tileViews = new Array2(boardWidth, boardHeight);

    this._actions = [];
    this._activeTweens = [];

    this._tweenManager = context.tweenManager;

    //events to listen for, the listener should be a function of the same name on the this object
    const HOOKS = this._HOOKS = [
      'onBoardTileAdd',
      'onBoardTileRemove',
      'onBoardTileSelect',
      'onBoardTileDeselect',
      'onBoardSwitch',
      'onBoardTileMove'
    ];

    const events = this._events;
    HOOKS.forEach((hook) => events[hook].addListener(this[hook], this));

    this._ticker.add(this.onTick, this);
  }

  destroy() {
    const events = this._events;
    const HOOKS = this._HOOKS;
    HOOKS.forEach((hook) => events[hook].removeListener(this[hook], this));
    this._tileViews.clear();

    this._ticker.remove(this.onTick, this);
    while (this._activeTweens.length > 0) {
      const tween = this._activeTweens.pop();
      tween.kill();
    }

    this._tileViews.clear();
  }

  onTick() {
    const actions = this._actions;
    const activeTweens = this._activeTweens;

    if (actions.length > 0 && activeTweens.length == 0) {
      const firstInLine = actions[0];
      while (actions.length > 0 && Object.getPrototypeOf(actions[0]) == Object.getPrototypeOf(firstInLine)) {
        const action = actions.shift();
        const tween = action.run();
        if (tween != null) activeTweens.push(tween);
      }
    }

    for (let i = activeTweens.length - 1; i >= 0; i--) {
      const tween = activeTweens[i];
      if (tween.totalProgress() >= 1) {
        tween.kill();
        activeTweens.splice(i, 1);
      }
    }
  }

  onClicked(event) {
    if (this._actions.length != 0 && this._activeTweens.length != 0) {
      //no input allowed while we're playing animations
      return;
    }
    const localPoint = event.data.getLocalPosition(this);
    const tileX = Math.floor(localPoint.x / this._tileSize);
    const tileY = Math.floor(localPoint.y / this._tileSize);

    this._events.onBoardTileClick.dispatch(tileX, tileY);
  }

  onBoardTileAdd(x, y, tileTypeId) {
    this._actions.push(new AddTileAction(this, x, y, tileTypeId));
  }

  onBoardTileRemove(x, y) {
    this._actions.push(new RemoveTileAction(this, x, y));
  }

  onBoardTileSelect(x, y) {
    this._actions.push(new SelectTileAction(this, x, y));
  }

  onBoardTileDeselect(x, y) {
    this._actions.push(new DeselectTileAction(this, x, y));
  }

  onBoardSwitch(thisX, thisY, thatX, thatY) {
    this._actions.push(new SwitchTileAction(this, thisX, thisY, thatX, thatY));
    //hack to prevent 2 consequtive switches from deadlocking
    this._actions.push(new Action());
  }

  onBoardTileMove(fromX, fromY, toX, toY) {
    this._actions.push(new MoveTileAction(this, fromX, fromY, toX, toY));
  }

  createTileViewForTileTypeId(tileTypeId) {
    const resources = this._resources;
    const texture = resources["gem" + (tileTypeId + 1)].texture
    return new TileView(this, texture, tileTypeId, this._tileSize);
  }

  createTweenForTileView(tileView, to, time) {
    const tileViewTween = TweenLite.to(tileView, time != null ? time : 1, to);
    return tileViewTween;
  }

  createTweenForTarget(target, to, time) {
    return TweenLite.to(target, time != null ? time : 1, to);
  }
}
