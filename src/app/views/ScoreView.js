"use strict";

const PIXI = require("pixi.js");
const ScoreViewUpdateAction = require("./actions/ScoreViewUpdateAction");

module.exports = class ScoreView extends PIXI.Text {
  constructor(context) {
    super("0", {
      fontFamily: ["Arial", "sans-serif"],
      fontSize: 64,
      fill: 0xFFFFFF,
      align: "center",
      stroke: 0x3e94dc,
      strokeThickness: 4
    });

    this._score = 0;

    this._actions = [];
    this._activeTweens = [];

    this._ticker = context.ticker;
    this._ticker.add(this.onTick, this);

    this._events = context.events;
    this._events.onBoardMatch.addListener(this.onMatch, this);
  }

  destroy() {
    this._ticker.remove(this.onTick, this);
    this._events.onBoardMatch.removeListener(this.onMatch, this);

    while (this._activeTweens.length > 0) {
      const tween = this._activeTweens.pop();
      tween.kill();
    }
  }

  onTick() {
    const actions = this._actions;
    const activeTweens = this._activeTweens;

    if (actions.length > 0 && activeTweens.length == 0) {
      const action = actions.shift();
      const tween = action.run();
      if (tween != null) activeTweens.push(tween);
    }

    for (let i = activeTweens.length - 1; i >= 0; i--) {
      const tween = activeTweens[i];
      if (tween.totalProgress() >= 1) {
        tween.kill();
        activeTweens.splice(i, 1);
      }
    }
  }

  onMatch(score) {
    this._actions.push(new ScoreViewUpdateAction(this, score));
  }

  incrementScore(scoreIncrement) {
    this._score += scoreIncrement;
    this.text = this._score;
  }
};
