"use strict"

const PIXI = require("pixi.js");
const TileView = require("./TileView");
const Array2 = require("../core/Array2");

module.exports = class BoardView extends PIXI.Container {
  constructor(context, boardHeight, boardWidth, tileSize) {
    super();

    this.interactive = true;
    this.buttonMode = true;

    this.pointerdown = this.onClicked.bind(this);

    this._resources = context.resources;
    this._tileSize = tileSize;

    this._tileViews = new Array2(boardWidth, boardHeight);

    //events to listen for, the listener should be a function of the same name on the this object
    const HOOKS = this._HOOKS = [
      'onBoardTileAdd',
      'onBoardTileRemove',
      'onBoardTileSelect',
      'onBoardTileDeselect',
      'onBoardSwitch',
      'onBoardTileMove'
    ];

    const events = this._events = context.events;
    HOOKS.forEach((hook) => events[hook].addListener(this[hook], this));
  }

  destroy() {
    const events = this._events;
    const HOOKS = this._HOOKS;
    HOOKS.forEach((hook) => events[hook].removeListener(this[hook], this));
    this._tileViews.clear();
  }

  onClicked(event) {
    const localPoint = event.data.getLocalPosition(this);
    const tileX = Math.floor(localPoint.x / this._tileSize);
    const tileY = Math.floor(localPoint.y / this._tileSize);

    this._events.onBoardTileClick.dispatch(tileX, tileY);
  }

  onBoardTileAdd(x, y, tileTypeId) {
    const newTileView = this.createTileViewForTileTypeId(tileTypeId);
    newTileView.setTilePosition(x, y);

    this._tileViews.set(x, y, newTileView);
    this.addChild(newTileView);
  }

  onBoardTileRemove(x, y) {
    const tileView = this._tileViews.get(x, y);
    this.removeChild(tileView);
  }

  onBoardTileSelect(x, y) {
    const tileView = this._tileViews.get(x, y);
    tileView.alpha = 0.5;
  }

  onBoardTileDeselect(x, y) {
    const tileView = this._tileViews.get(x, y);
    tileView.alpha = 1;
  }

  onBoardSwitch(thisX, thisY, thatX, thatY) {
    const tiles = this._tileViews;

    const thisTileView = tiles.get(thisX, thisY);
    const thatTileView = tiles.get(thatX, thatY);

    tiles.set(thatX, thatY, thisTileView);
    tiles.set(thisX, thisY, thatTileView);

    thisTileView.setTilePosition(thatX, thatY);
    thatTileView.setTilePosition(thisX, thisY);
  }

  onBoardTileMove(fromX, fromY, toX, toY) {
    const tiles = this._tileViews;

    const tileView = tiles.get(fromX, fromY);

    tiles.move(fromX, fromY, toX, toY);

    tileView.setTilePosition(toX, toY);
  }

  createTileViewForTileTypeId(tileTypeId) {
    const resources = this._resources;
    const texture = resources["gem" + (tileTypeId + 1)].texture
    return new TileView(this, texture, tileTypeId, this._tileSize);
  }
}
