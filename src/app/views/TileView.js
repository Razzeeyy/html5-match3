"use strict";

const PIXI = require('pixi.js');

module.exports = class TileView extends PIXI.Sprite {
  constructor(boardView, texture, tileTypeId, tileSize) {
    super(texture);

    this._boardView = boardView;
    this._tileTypeId = tileTypeId;
    this._tileSize = tileSize;
  }

  get tileX() {
    return this._tileX;
  }

  set tileX(value) {
    this._tileX = value;
    this.x = value * this._tileSize;
  }

  get tileY() {
    return this._tileY;
  }

  set tileY(value) {
    this._tileY = value;
    this.y = value * this._tileSize;
  }

  setTilePosition(tileX, tileY) {
    this.tileX = tileX;
    this.tileY = tileY;
  }

  removeFromBoardView() {
    this._boardView.removeChild(this);
  }

};
