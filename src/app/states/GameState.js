"use strict";

const State = require('../core/State');
const BoardModel = require('../models/BoardModel');
const BoardView = require('../views/BoardView');

module.exports = class GameState extends State {
  constructor() {
    super();

    this._background = null;
    this._boardModel = null;
    this._boardView = null;
  }

  onEnter(context) {
    const PIXI = context.pixi;
    const stage = context.stage;
    const resources = context.resources;
    const world = context.world;

    const background = this._background = new PIXI.Sprite(resources.background.texture);
    stage.addChild(background);

    const boardWidth = world.boardUnitWidth;
    const boardHeight = world.boardUnitHeight;
    const tileTypes = world.tileColors;
    const tileSize = world.pixelsPerUnit;

    const boardModel = this._boardModel = new BoardModel(context, boardWidth, boardHeight, tileTypes);

    const boardView = this._boardView = new BoardView(context, boardWidth, boardHeight, tileSize);
    stage.addChild(boardView);

    boardModel.populate();
  }

  onResize(context) {
    const screen = context.screen;
    this._background.width = screen.width;
    this._background.height = screen.height;

    if (screen.width < screen.height) { //portrait
      const boardSize = screen.width*0.9;
      this._boardView.x = (screen.width - boardSize) * 0.5;
      this._boardView.y = (screen.height - boardSize) * 0.5;
      this._boardView.width = boardSize;
      this._boardView.height = boardSize;
    } else { //landscape
      const boardSize = screen.height*0.9;
      this._boardView.x = (screen.width - boardSize) * 0.5;
      this._boardView.y = (screen.height - boardSize) * 0.5;
      this._boardView.width = boardSize;
      this._boardView.height = boardSize;
    }

    console.log(this._boardView.width, this._boardView.height);
  }

  onLeave(context) {
    const stage = context.stage;
    stage.removeChild(this._background);
    stage.removeChild(this._boardView);

    this._boardView.destroy();
    this._boardModel.destroy();
  }
};
