"use strict";

const Signal = require('../core/Signal');

module.exports = Object.freeze({
  onBoardTileClick: new Signal(), //when user clicks on the position on gamefield, this event is emitted

  onBoardPopulate: new Signal(), //when board is initially populated this even is emitted
  onBoardTileAdd: new Signal(), //emitted when new tile added to the board, may be emitted after populate/refill/undo

  onBoardTileSelect: new Signal(), //emitted to notify which tile was selected as a result of a previous click event
  onBoardTileDeselect: new Signal(), //emitted to notify that tile was deselected

  onBoardSwitch: new Signal(), //emitted to notify that some tiles should be visually switched
  onBoardTileMove: new Signal(), //emitted when tile moves, may be emitted after fall

  onBoardMatch: new Signal(), //emitted when there is a succesfull match on the board
  onBoardTileRemove: new Signal(), //emitted when tile removed from the board, may be emitted after match

  onBoardFall: new Signal(), //emitted when tiles start to fall as a subject of gravity, followed by tile move events

  onBoardRefill: new Signal(), //emitted when new tiles introduced to the board to fill up the space left as a resulf of a succesfull match, followed by the tile add events

  onBoardShuffle: new Signal(), //emitted when board detected no more possible matches and reshuffled, followed by tile move events
});
