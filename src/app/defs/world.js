"use strict";

module.exports = Object.freeze({
  pixelsPerUnit: 128,

  boardUnitWidth: 6,
  boardUnitHeight: 6,

  tileColors: 4,
});
