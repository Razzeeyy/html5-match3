"use strict";

module.exports = Object.freeze({
  "gem1": "gem1.png",
  "gem2": "gem2.png",
  "gem3": "gem3.png",
  "gem4": "gem4.png",
  "gem5": "gem5.png",
  "gem6": "gem6.png",
  "background": "level_background.png"
});
