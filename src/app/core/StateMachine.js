"use strict";

//simple finite state machine implementation
//in contrast to StateManager this has no mandatory methods, and requires all states to be pre-registered
module.exports = class StateMachine {
  constructor() {
    this._states = {};
    this._currentState = null;
  }

  addState(name, state) {
    if (!name) throw new Error('Invalid Name');
    if (!state) throw new Error('Invalid State');
    if (this._states[name]) throw new Error('State Already Exists: ' + name);
    this._states[name] = state;
  }

  setState(name) {
    if (!this._states[name]) throw new Error('State Not Found: ' + name);
    this._currentState = this._states[name];
  }

  getState() {
    return this._currentState;
  }

  invoke(methodName, ...args) {
    if (this._currentState != null && this._currentState[methodName] != null) this._currentState[methodName].apply(this._currentState, args);
  }
}
