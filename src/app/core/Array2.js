"use strict";

module.exports = class Array2 {
  constructor(width, height) {
    this._values = [];

    //array gets initialized with nulls for safety and potential performance reasons
    this.resize(width, height);
    this.clear();
  }

  clear() {
    for (let i = 0; i < this.width * this.height; i++) {
      this._values[i] = null;
    }
  }

  get width() {
    return this._width;
  }

  get height() {
    return this._height;
  }

  resize(width, height) { //warning, make sure to clear array after resize if you don't want to misinterpret it's content
    this._width = width;
    this._height = height;
  }

  set(x, y, value) {
    if (!this.contains(x, y)) throw new Error("Out of Bounds");

    this._values[this.index(x, y)] = value;
  }

  get(x, y) {
    if (!this.contains(x, y)) throw new Error("Out of Bounds");

    return this._values[this.index(x, y)];
  }

  move(fromX, fromY, toX, toY) {
    if (!this.contains(fromX, fromY)) throw new Error("Out of Bounds");
    if (!this.contains(toX, toY)) throw new Error("Out of Bounds");

    let value = this.get(fromX, fromY);
    this.set(toX, toY, value);
    this.set(fromX, fromY, null);
  }

  contains(x, y) {
    return (x >= 0 && x < this._width) && (y >= 0 && y < this._height);
  }

  index(x, y) {
    return y * this._width + x;
  }
}
