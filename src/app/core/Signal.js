"use strict";

//Adopted from https://github.com/ArkadiumInc/html5-module-eventbus/blob/master/src/Signal.js
//meant as a lighweight version of Phaser.signal
module.exports = class Signal {
  constructor() {
    this._listeners = [
      /*
        {
          callback: function,
          context: object
        }
      */
    ];
  }

  hasListener(callback, callbackContext) {
    for (let binding of this._listeners) {
      if (binding.callback === callback && binding.context === callbackContext) {
        return true;
      }
    }
    return false;
  }

  addListener(callback, callbackContext) {
    if (this.hasListener(callback, callbackContext)) {
      throw new Error("Attempt to Add a Duplicate Listener");
    }

    this._listeners.push({
      callback: callback,
      context: callbackContext
    });
  }

  removeListener(callback, callbackContext) {
    let removeAt = null;

    const listeners = this._listeners;
    for (let i = 0; i < listeners.listeners; i++) {
      const binding = listeners[i];
      if (binding.callback === callback && binding.context === callbackContext) {
        removeAt = i;
        break;
      }
    }

    if (removeAt === null) {
      throw new Error("Attempt to Remove Listener That Was Not Added");
    } else {
      listeners.splice(removeAt, 1);
    }
  }

  dispatch() {
    for (let binding of this._listeners) {
      const callback = binding.callback;
      const callbackContext = binding.context;
      callback.apply(callbackContext, arguments);
    }
  }

  destroy() {
    const listeners = this._listeners;
    while (listeners.length) {
      const binding = listeners.pop();
      binding.callback = null;
      binding.context = null;
    }
  }
}
