"use strict";

module.exports = class Random {
  constructor() {

  }

  randomFloat(min, max) { //note: max non inclusive
    return Math.random() * (max - min) + min;
  }

  randomInt(min, max) { //note: max non inclusive
    return Math.floor(this.randomFloat(min, max));
  }

  randomSign() {
    return Math.random() < 0.5 ? -1 : 1;
  }
}
