"use strict";

module.exports = class StateManager {
  constructor(context) {
    this._state = null;
    this._context = context;
  }

  setState(newState) {
    if (this._state != null) this._state.onLeave(this._context);
    this._state = newState;
    this._state.onEnter(this._context);
    this._state.onResize(this._context);
  }

  invoke(methodName, ...args) {
    args.unshift(this._context);
    if (this._state != null && this._state[methodName] != null) this._state[methodName].apply(this._state, args);
  }
}
