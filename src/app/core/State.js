"use strict";

module.exports = class State {
  constructor() {

  }

  onEnter(context) {
    throw new Error("Override Me");
  }

  onResize(context) {
    throw new Error("Override Me");
  }

  onLeave(context) {
    throw new Error("Override Me");
  }
}
