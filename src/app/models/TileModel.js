"use strict";

module.exports = class TileModel {
  constructor(x, y, tileId) {
    this.x = x;
    this.y = y;
    this.tileId = tileId;
  }

  clone() {
    return new TileModel(this.x, this.y, this.tileId);
  }

  equals(other) {
    return other.x == this.x && other.y == this.y && other.tileId == this.tileId;
  }
}
