"use strict";

const Array2 = require("../../core/Array2");
const world = require("../../defs/world");

module.exports = class BoardModelMatchState {
  constructor() {
    this._matches = new Array2(world.boardUnitWidth, world.boardUnitHeight); //just use the world board defs as a safe values to begin with
    this._scores = {};
  }

  onTick(board) {
    const matches = this._matches;
    if (matches.width != board.width || matches.height != board.height) //in case our current board is of different size
      matches.resize(board.width, board.height); //adopt the current's board size

    matches.clear();

    const tiles = board._tiles;
    let anyMatches = false;

    //at first mark all the matches
    for (let x = 0; x < tiles.width; x++) {
      for (let y = 0; y < tiles.height; y++) {
        if (x >= 2) {
          const x1 = tiles.get(x - 1, y);
          const x2 = tiles.get(x - 2, y);
          const x3 = tiles.get(x, y);

          if (x1 != null && x2 != null && x3 != null &&
            x1.tileId == x2.tileId && x2.tileId == x3.tileId) {
            matches.set(x1.x, x1.y, x1);
            matches.set(x2.x, x2.y, x2);
            matches.set(x3.x, x3.y, x3);
            anyMatches = true;
          }
        }

        if (y >= 2) {
          const y1 = tiles.get(x, y - 1);
          const y2 = tiles.get(x, y - 2);
          const y3 = tiles.get(x, y);

          if (y1 != null && y2 != null && y3 != null &&
            y1.tileId == y2.tileId && y2.tileId == y3.tileId) {
            matches.set(y1.x, y1.y, y1);
            matches.set(y2.x, y2.y, y2);
            matches.set(y3.x, y3.y, y3);
            anyMatches = true;
          }
        }
      }
    }

    //if there was any matches lets score them and emit events
    if (anyMatches) {
      const score = this._calculateScore();
      board._events.onBoardMatch.dispatch(score);

      for (let x = 0; x < matches.width; x++) {
        for (let y = 0; y < matches.height; y++) {
          const tile = matches.get(x, y);
          if (tile != null) {
            board.removeTile(tile);
          }
        }
      }
    }

    board._states.setState("fall");
  }

  _calculateScore() { //FIXME: this is untested and may not work properly
    const scores = this._scores;
    const matches = this._matches;

    for(const key of Object.keys(scores)) { //make sure to nullify the scores
      scores[key] = 0;
    }

    for(let x=0; x<matches.width; x++) {
      for(let y=0; y<matches.height; y++) {
        const tile = matches.get(x, y);
        if(tile != null) {
          scores[tile.tileId] = (scores[tile.tileId] || 0) + 1;
        }
      }
    }

    let score = 0;

    for(const key of Object.keys(scores)) {
      const tiles = scores[key];
      if(tiles >= 3) {
        score += 10 + (tiles-3) * 5;
      }
    }

    return score;
  }
}
