"use strict";

module.exports = class BoardModelRefillState {
  constructor() {

  }

  onTick(board) {
    const random = board._random;
    const events = board._events;
    const tileTypes = board._tileTypes;

    const tiles = board._tiles;

    events.onBoardRefill.dispatch();

    for (let x = 0; x < tiles.width; x++) {
      for (let y = 0; y < tiles.height; y++) {
        if (tiles.get(x, y) == null) { //if it's an empty cell
          const newTileId = random.randomInt(0, tileTypes);
          board.createTile(x, y, newTileId); //populate with a random new tile
        }
      }
    }

    if (board.isMatchAnywhere()) {
      board._states.setState("match");
    } else {
      board._states.setState("reshuffle");
    }
  }
}
