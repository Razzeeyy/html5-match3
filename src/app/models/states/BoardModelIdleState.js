"use strict";

module.exports = class BoardModelIdleState {
  constructor() {
    this._selected = [];
  }

  onTick(board) {
    const selected = this._selected;

    if (selected.length == 2) {
      const thatTile = selected[0];
      const thisTile = selected[1];

      //lets try match or switch back
      if (board.isMatchAnywhere()) { //if there is no matches, switch back
        board._states.setState("match");
      } else {
        board.switchTiles(thatTile, thisTile);
      }

      selected.length = 0;
    }
  }

  onTileClick(board, x, y) {
    const tiles = board._tiles;
    const clickedTile = tiles.get(x, y);
    const selected = this._selected;

    switch (selected.length) {
      case 0:
        {
          if (clickedTile != null) {
            selected.push(clickedTile);
            board.selectTile(clickedTile);
          }
          break;
        }
      case 1:
        {
          const previousSelected = selected[0];

          if (clickedTile == null || previousSelected.equals(clickedTile))
            break; //no op if we try to click already selected tile or empty tile

          board.deselectTile(previousSelected);

          const distanceSquared = Math.pow(previousSelected.x - x, 2) + Math.pow(previousSelected.y - y, 2);
          if (distanceSquared > 1) { //too far from the current tile, select the new clicked candidate instead
            selected.length = 0;
            selected.push(clickedTile);
            board.selectTile(clickedTile);
            break;
          }

          //we're close enough for a switch, lets switch
          selected.push(clickedTile);
          board.switchTiles(previousSelected, clickedTile);

          break;
        }
      case 2:
        //this case is checked in onTick
        break;
      default:
        throw new Error("Invalid Selection State");
    }
  }
}
