"use strict";

module.exports = class BoardModelFallState {
  constructor() {

  }

  onTick(board) {
    let fallEmitted = false;

    //NOTE: code below assumes gravity pulling (visually) upwards
    const tiles = board._tiles;
    for (let x = 0; x < tiles.width; x++) {
      for (let y = 0; y < tiles.height; y++) {
        if (tiles.get(x, y) == null) { //empty tile, lets find something below it to fill it
          let offset = 1;
          while (y + offset < tiles.height && tiles.get(x, y + offset) == null) {
            offset++;
          }
          if (y + offset < tiles.height) {
            const tileToFall = tiles.get(x, y + offset);
            if (tileToFall != null) {
              if (!fallEmitted) {
                board._events.onBoardFall.dispatch();
              }
              board.moveTile(tileToFall, x, y);
            }
          }
        }
      }
    }

    board._states.setState("refill");
  }
}
