"use strict";

const Array2 = require("../core/Array2");
const TileModel = require("./TileModel");
const StateMachine = require("../core/StateMachine");
const BoardModelIdleState = require("./states/BoardModelIdleState");
const BoardModelMatchState = require("./states/BoardModelMatchState");
const BoardModelFallState = require("./states/BoardModelFallState");
const BoardModelRefillState = require("./states/BoardModelRefillState");
const BoardModelReshuffleState = require("./states/BoardModelReshuffleState");

module.exports = class BoardModel {
  constructor(context, width, height, tileTypes) {
    this._events = context.events;
    this._ticker = context.ticker;
    this._random = context.random;

    this._tileTypes = tileTypes;
    this._tiles = new Array2(width, height);

    this._states = new StateMachine();
    this._states.addState("idle", new BoardModelIdleState());
    this._states.addState("match", new BoardModelMatchState());
    this._states.addState("fall", new BoardModelFallState());
    this._states.addState("refill", new BoardModelRefillState());
    this._states.addState("reshuffle", new BoardModelReshuffleState());

    this._events.onBoardTileClick.addListener(this.onTileClick, this);
    this._ticker.add(this.onTick, this);

    this._states.setState("idle");
  }

  destroy() {
    this._events.onBoardTileClick.removeListener(this.onTileClick, this);
    this._ticker.remove(this.onTick, this);
    this._tiles.clear();
  }

  onTick() {
    this._states.invoke("onTick", this);
  }

  onTileClick(x, y) {
    this._states.invoke("onTileClick", this, x, y);
  }

  get width() {
    return this._tiles.width;
  }

  get height() {
    return this._tiles.height;
  }

  populate() {
    const random = this._random;
    const events = this._events;
    const tileTypes = this._tileTypes;

    const tiles = this._tiles;

    events.onBoardPopulate.dispatch();

    //match-free board generation algorithm adopted from http://answers.unity3d.com/answers/1153741/view.html
    //oh thank you, wise guys from the internets, you're the titans to build on
    let newTileId = null;
    let tryDirection = 1;

    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        newTileId = random.randomInt(0, tileTypes); //a tile type to start with
        tryDirection = random.randomSign(); //randomize id shift direction to remove possible recurring patterns on non-match tile tryouts

        do { //try out the match-free tile
          //note: the final tile type will always be at least one away from the random above
          //moving by (tryDirection) 1 or -1 each step and wrapping around if over max tile type id tries to omptimize for constant time
          //to prevent many failed try-out attempts in a row as a result of pure random roll for a new tile type
          newTileId = (newTileId + tryDirection) % tileTypes;
          if (newTileId < 0) newTileId = tileTypes - 1;
        } while (
          (x >= 2 && tiles.get(x - 1, y).tileId == newTileId && tiles.get(x - 2, y).tileId == newTileId) ||
          (y >= 2 && tiles.get(x, y - 1).tileId == newTileId && tiles.get(x, y - 2).tileId == newTileId)
        )

        this.addTile(new TileModel(x, y, newTileId));
      }
    }
  }

  isMatchAnywhere() {
    const tiles = this._tiles;
    for (let x = 0; x < tiles.width; x++) {
      for (let y = 0; y < tiles.height; y++) {
        if (x >= 2) {
          const x1 = tiles.get(x - 1, y);
          const x2 = tiles.get(x - 2, y);
          const x3 = tiles.get(x, y);

          if (x1 != null && x2 != null && x3 != null &&
            x1.tileId == x2.tileId && x2.tileId == x3.tileId)
            return true;
        }

        if (y >= 2) {
          const y1 = tiles.get(x, y - 1);
          const y2 = tiles.get(x, y - 2);
          const y3 = tiles.get(x, y);

          if (y1 != null && y2 != null && y3 != null &&
            y1.tileId == y2.tileId && y2.tileId == y3.tileId)
            return true;
        }
      }
    }

    return false;
  }

  //maybe name is a bit weird, but this creates and adds tile to the board
  //this meant to hide some details about tilemodel implementation from board states
  createTile(x, y, tileId) {
    this.addTile(new TileModel(x, y, tileId));
  }

  addTile(tile) {
    this._tiles.set(tile.x, tile.y, tile);
    this._events.onBoardTileAdd.dispatch(tile.x, tile.y, tile.tileId);
  }

  selectTile(tile) {
    this._events.onBoardTileSelect.dispatch(tile.x, tile.y, tile.tileId);
  }

  deselectTile(tile) {
    this._events.onBoardTileDeselect.dispatch(tile.x, tile.y, tile.tileId);
  }

  removeTile(tile) {
    this._tiles.set(tile.x, tile.y, null);
    this._events.onBoardTileRemove.dispatch(tile.x, tile.y, tile.tileId);
  }

  moveTile(tile, toX, toY) {
    const fromX = tile.x;
    const fromY = tile.y;

    const tiles = this._tiles;
    tiles.move(fromX, fromY, toX, toY);
    tile.x = toX;
    tile.y = toY;

    this._events.onBoardTileMove.dispatch(fromX, fromY, toX, toY, tile.tileId);
  }

  switchTiles(thisTile, thatTile) {
    const tiles = this._tiles;
    const events = this._events;

    tiles.set(thisTile.x, thisTile.y, thatTile);
    tiles.set(thatTile.x, thatTile.y, thisTile);

    let mem = thisTile.x;
    thisTile.x = thatTile.x;
    thatTile.x = mem;

    mem = thisTile.y;
    thisTile.y = thatTile.y;
    thatTile.y = mem;

    events.onBoardSwitch.dispatch(thatTile.x, thatTile.y, thisTile.x, thisTile.y, thatTile.tileId, thisTile.tileId);
  }
};
